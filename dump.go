package bpt

import (
	"fmt"
	"io"
	"os"
)

// DumpTree outputs the keys of each vertex in a tree to standard out. An
// internal node is represented as "node: a b c". A leaf node is represented as
// "leaf: d e f".
func DumpTree(t *Tree) {
	outputTree(t, os.Stdout)
}

type nodeBacklog struct {
	node         vertex
	nextSubIndex int // index to next backtrack point. must be >= 1.
	w            io.Writer
}

func outputTree(t *Tree, w io.Writer) {
	level := 0
	curr := t.root
	var nbl nodeBacklog
	nbl.w = w
	ptr := &nbl
	var backlogStack [maxLevel]nodeBacklog
	top := 0 // index in backlogStack

	for true {
		if curr == nil {
			if top == 0 {
				ptr = nil
			} else {
				top--
				ptr = &backlogStack[top]
			}

			if ptr == nil {
				break // GTFO
			}
			curr = ptr.node
			level--
		} else {
			subInd := 0
			if ptr != nil {
				subInd = ptr.nextSubIndex
			}
			ptr = nil

			// backlog the path
			if isLeaf(&curr) || subInd+1 >= childCount(curr) {
				backlogStack[top].node = nil
				backlogStack[top].nextSubIndex = 0
			} else {
				backlogStack[top].node = curr
				backlogStack[top].nextSubIndex = subInd + 1
			}

			top++
			level++

			// draw whole node when 1st entry passed through
			if subInd == 0 {
				for i := 1; i < level; i++ {
					if i == level-1 {
						fmt.Fprintf(nbl.w, "%-8s", "+-------")
						continue
					}

					if backlogStack[i-1].node != nil {
						fmt.Fprintf(nbl.w, "%-8s", "|")
					} else {
						fmt.Fprintf(nbl.w, "%-8s", " ")
					}
				}

				printKeys(curr, nbl.w)
			}

			// move deep down
			if isLeaf(&curr) {
				curr = nil
			} else {
				node := curr.(*internalNode)
				curr = node.nodes[subInd]
			}
		}
	}
}

func childCount(v vertex) int {
	switch node := v.(type) {
	case *internalNode:
		return node.count
	case *leafNode:
		return node.count
	default:
		err := fmt.Errorf("unknown type %T for value %v", node, node)
		panic(err)
	}
}

func printKeys(v vertex, w io.Writer) {
	if isLeaf(&v) {
		leaf := v.(*leafNode)
		count := leaf.Count()
		fmt.Fprintf(w, "leaf:")
		for _, key := range leaf.keys[:count] {
			fmt.Fprintf(w, " %d", key)
		}
		fmt.Fprintf(w, "\n")
		return
	}

	node := v.(*internalNode)
	count := node.Count() - 1 // always 1 less key than number of children
	fmt.Fprintf(w, "node:")
	for _, key := range node.keys[:count] {
		fmt.Fprintf(w, " %d", key)
	}
	fmt.Fprintf(w, "\n")
}
