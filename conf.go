package bpt

import "fmt"

const (
	minOrder   = 3
	maxOrder   = 64
	maxEntries = 64
	maxLevel   = 10
)

func init() {
	if maxOrder <= minOrder {
		err := fmt.Errorf("internal constant maxOrder (%d) must be greater than internal constant minOrder (%d)", maxOrder, minOrder)
		panic(err)
	}
	if maxLevel < 1 {
		err := fmt.Errorf("internal constant maxLevel (%d) is too low", maxLevel)
		panic(err)
	}
	if maxEntries < 1 {
		err := fmt.Errorf("internal constant maxEntries (%d) is too low", maxEntries)
		panic(err)
	}
}
