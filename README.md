# bpt

[![GoDoc](https://godoc.org/bitbucket.org/rafaelespinoza/bpt?status.svg)](https://godoc.org/bitbucket.org/rafaelespinoza/bpt)

This is an implementation of a B Plus Tree. It started as an exercise to help me understand how some indexes work in relational databases. It draws heavily from these C-based implementations:

- [Interactive B+ Tree](http://www.amittai.com/prose/bplustree.html).
- [github.com/begeekmyfriend/bplustree](https://github.com/begeekmyfriend/bplustree/tree/in-memory).
