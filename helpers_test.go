package bpt

import (
	"encoding/json"
	"fmt"
	"math/rand"
	"strings"
	"testing"
)

// test helper functions go here

func testNodeSibling(t *testing.T, node vertex, dir siblingDirection, expected vertex) bool {
	t.Helper()
	switch v := node.(type) {
	case *internalNode:
		exp := expected.(*internalNode)
		if dir == siblingToLeft && v.prev() != exp {
			t.Errorf("prev %v != expected %v", v.prev(), exp)
			return false
		} else if dir == siblingToRight && v.next() != exp {
			t.Errorf("next %v != expected %v", v.next(), exp)
			return false
		}
	case *leafNode:
		exp := expected.(*leafNode)
		if dir == siblingToLeft && v.prev() != exp {
			t.Errorf("prev %v != expected %v", v.prev(), exp)
			return false
		} else if dir == siblingToRight && v.next() != exp {
			t.Errorf("next %v != expected %v", v.next(), exp)
			return false
		}
	default:
		err := fmt.Errorf("unknown type %T %v", v, v)
		panic(err)
	}

	return true
}

func testNodeSiblings(t *testing.T, visibleNodes []vertex) bool {
	t.Helper()
	for i, ptr := range visibleNodes {
		parentKeyInd := ptr.ParentKeyInd()
		if parentKeyInd != i-1 {
			t.Errorf("\tWrong parent key index. Got %d expected %d", parentKeyInd, i-1)
			return false
		}
		if i > 0 {
			if ok := testNodeSibling(t, ptr, siblingToLeft, visibleNodes[i-1]); !ok {
				t.Logf("Left sibling for node w/ parentKeyInd %d incorrect", parentKeyInd)
				return false
			}
		}
		if i < len(visibleNodes)-1 {
			if ok := testNodeSibling(t, ptr, siblingToRight, visibleNodes[i+1]); !ok {
				t.Logf("Right sibling for node w/ parentKeyInd %d incorrect", parentKeyInd)
				return false
			}
		}
	}
	return true
}

func testNode(t *testing.T, node vertex) bool {
	t.Helper()
	numEntries := node.Count()
	if !isLeaf(&node) {
		numEntries--
	}
	entries := nodeEntries(node)
	pki := node.ParentKeyInd()
	parent := node.Parent()
	left := leftNode(node)
	right := rightNode(node)
	if parent == nil {
		if pki != -1 {
			t.Error("Root node parent key index should be -1")
			return false
		}
		if left != nil && left == node {
			t.Error("Root node cannot have siblings")
			mustPrettify(t, node)
			mustPrettify(t, left)
			return false
		}
		if right != nil && right == node {
			t.Error("Root node cannot have siblings")
			mustPrettify(t, node)
			mustPrettify(t, right)
			return false
		}
	} else {
		if nodeEmpty(left) {
			if ok := testLeftMostSibling(t, node); !ok {
				return false
			}
		} else {
			if pki > 0 && pki != left.ParentKeyInd()+1 {
				t.Error("parent key index should be +1 of left sibling")
				return false
			}
			// check current node's first entry is >= left node's last entry, if possible
			c := left.Count()
			if !isLeaf(&left) {
				c--
			}
			if numEntries > 0 && c > 0 && entries[0].Less(nodeEntries(left)[c-1]) {
				t.Error("first entry should be >= last entry of left sibling")
				return false
			}
		}
		if nodeEmpty(right) {
			if ok := testRightMostSibling(t, node); !ok {
				return false
			}
		} else {
			if pki < parent.Count()-2 && pki != right.ParentKeyInd()-1 {
				t.Error("parent key index should be -1 of right sibling")
				return false
			}
			// check current node's last entry is < right node's first entry, if possible
			if numEntries > 0 && right.Count() > 0 && !entries[numEntries-1].Less(nodeEntries(right)[0]) {
				t.Error("last entry should be < first entry of right sibling")
				return false
			}
		}
	}

	if ok := testEntriesSorted(t, entries, numEntries); !ok {
		return false
	}

	return true
}

func testTreeNodesFromRoot(t *testing.T, root vertex) bool {
	t.Helper()
	visited := make(map[vertex]bool)
	q := []vertex{root}
	i := 0
	for len(q) > 0 {
		var curr vertex
		curr, q = q[0], q[1:]
		visited[curr] = true
		i++
		if ok := testNode(t, curr); !ok {
			return false
		}
		if !isLeaf(&curr) {
			node := curr.(*internalNode)
			for _, ch := range node.nodes[:node.count] {
				if _, ok := visited[ch]; ok {
					t.Error("Should not visit same node twice")
					return false
				}
				q = append(q, ch)
			}
		}
	}
	return true
}

func testLeftMostSibling(t *testing.T, node vertex) bool {
	t.Helper()
	curr := node

	for curr != nil {
		if isLeaf(&curr) {
			n := curr.(*leafNode)
			if n == nil {
				break
			}
			if n.prev() != nil {
				t.Errorf("Expected %T.prev to be nil, got %v", n, n.prev())
				return false
			}
		} else {
			n := curr.(*internalNode)
			if n == nil {
				break
			}
			if n.prev() != nil {
				t.Errorf("Expected %T.prev to be nil, got %v", n, n.prev())
				return false
			}
		}
		if curr.ParentKeyInd() != -1 {
			t.Errorf("Expected ParentKeyInd() to be -1, got %d", curr.ParentKeyInd())
			return false
		}
		curr = curr.Parent()
	}

	return true
}

func testRightMostSibling(t *testing.T, node vertex) bool {
	t.Helper()
	curr := node

	for curr != nil {
		if isLeaf(&curr) {
			n := curr.(*leafNode)
			if n == nil {
				break
			}
			if n.next() != nil {
				t.Errorf("Expected %T.next to be nil, got %v", n, n.next())
				return false
			}
		} else {
			n := curr.(*internalNode)
			if n == nil {
				break
			}
			if n.next() != nil {
				t.Errorf("Expected %T.next to be nil, got %v", n, n.next())
				return false
			}
		}
		parent := curr.Parent()
		if parent == nil {
			curr = parent
		} else if curr.ParentKeyInd() != parent.Count()-2 {
			t.Errorf("Expected ParentKeyInd() to be %d, got %d", parent.Count()-2, curr.ParentKeyInd())
			return false
		}
		curr = parent
	}

	return true
}

func testEntriesSorted(t *testing.T, entries []Entry, numEntries int) bool {
	t.Helper()
	for i := numEntries - 1; i > 0; i-- {
		if entries[i] != nil && entries[i].Less(entries[i-1]) {
			t.Errorf("entries not sorted. entries[%d] < entries[%d], %v < %v", i, i-1, entries[i], entries[i-1])
			return false
		}
	}
	return true
}

func nodeEmpty(node vertex) bool {
	switch v := node.(type) {
	case *leafNode:
		return v == nil
	case *internalNode:
		return v == nil
	default:
		err := fmt.Errorf("unknown type %T %v", v, v)
		panic(err)
	}
}

func nodeEntries(node vertex) []Entry {
	switch n := node.(type) {
	case *leafNode:
		return n.keys
	case *internalNode:
		return n.keys
	default:
		err := fmt.Errorf("unknown type %T %v", n, n)
		panic(err)
	}
}

func leftNode(node vertex) vertex {
	if isLeaf(&node) {
		n := node.(*leafNode)
		return n.prev()
	}
	n := node.(*internalNode)
	return n.prev()
}

func rightNode(node vertex) vertex {
	if isLeaf(&node) {
		n := node.(*leafNode)
		return n.next()
	}
	n := node.(*internalNode)
	return n.next()
}

func mustPrettify(t *testing.T, v vertex) {
	t.Helper()
	if out, err := prettyNodeJSON(v); err != nil {
		panic(err)
	} else {
		t.Logf("%v\n", string(out))
	}
}

func dumpTreeInTestContext(testContext *testing.T, tree *Tree) {
	var b strings.Builder
	outputTree(tree, &b)
	testContext.Helper()
	testContext.Logf("\n%s\n", b.String())
}

func prettyPrintNodeJSON(v vertex) error {
	out, err := prettyNodeJSON(v)
	if err != nil {
		return err
	}
	fmt.Println(string(out))
	return nil
}

type nodeJSON struct {
	ParentKeyInd int     `json:"parentKeyInd"`
	Count        int     `json:"count"`
	Keys         []Entry `json:"keys"`
}

func prettyNodeJSON(v vertex) (out []byte, err error) {
	switch val := v.(type) {
	case *leafNode:
		j := nodeJSON{
			ParentKeyInd: val.parentKeyInd,
			Count:        val.count,
			Keys:         val.keys[:val.count],
		}
		out, err = json.MarshalIndent(&j, "", "    ")
	case *internalNode:
		j := nodeJSON{
			ParentKeyInd: val.parentKeyInd,
			Count:        val.count,
			Keys:         val.keys[:val.count],
		}
		out, err = json.MarshalIndent(&j, "", "    ")
	default:
		err = fmt.Errorf("unexpected value %v of type %T", val, val)
	}
	return
}

type testRecord struct {
	Int
	val bool
}

func (t testRecord) Less(u Entry) bool {
	switch b := u.(type) {
	case testRecord:
		a := t.Int
		return a.Less(b.Int)
	case nil:
		return true // put zero-values at the end
	default:
		err := fmt.Errorf("unknown type %T %v", b, b)
		panic(err)
	}
}

func makeIntRange(lo, hi int) []Int {
	rng := make([]Int, 0)
	for i := lo; i <= hi; i++ {
		rng = append(rng, Int(i))
	}
	return rng
}

func makeIntRangeStep(lo, hi, step int) []Int {
	rng := make([]Int, 0)
	for i := lo; i <= hi; i = i + step {
		rng = append(rng, Int(i))
	}
	return rng
}

// makePerm returns a random permutation of n integers in the range [0, n).
func makePerm(n int) (out []Entry) {
	for _, v := range rand.Perm(n) {
		out = append(out, Int(v))
	}
	return
}

type byInts []Entry

func (a byInts) Len() int {
	return len(a)
}

func (a byInts) Less(i, j int) bool {
	return a[i].(Int) < a[j].(Int)
}

func (a byInts) Swap(i, j int) {
	a[i], a[j] = a[j], a[i]
}
