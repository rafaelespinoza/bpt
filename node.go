package bpt

import (
	"container/list"
	"fmt"
	"sort"
)

// vertex is an interface for nodes in a tree.
type vertex interface {
	ParentKeyInd() int
	SetParentKeyInd(i int)
	Parent() *internalNode
	SetParent(n *internalNode)
	Link() *link
	Count() int
}

var _ vertex = (*internalNode)(nil)
var _ vertex = (*leafNode)(nil)

// An internalNode is any non-leaf node in a tree. The parentKeyInd field
// represents its place relative to any sibling internalNodes with respect to
// its parent. A value of -1 means it is the left-most child of its parent. The
// keys field is for "directing traffic" in searches from the tree root. The
// nodes field is a list of pointers to children nodes. The length of the keys
// and nodes fields do not change; instead values start out as nil and are
// replaced, swapped, copied as needed. The relevant, or active, values will be
// in the range [0, count].
type internalNode struct {
	parentKeyInd int
	parent       *internalNode
	link         *link
	count        int // number of active values in 'nodes' field
	keys         []Entry
	nodes        []vertex
}

func newInternalNode(numKeys int) *internalNode {
	var node internalNode
	node.parentKeyInd = -1
	// The link field is assigned when added to Tree's list
	node.keys = make([]Entry, numKeys)
	node.nodes = make([]vertex, numKeys+1)
	return &node
}

func (n *internalNode) ParentKeyInd() int {
	return n.parentKeyInd
}

func (n *internalNode) SetParentKeyInd(i int) {
	n.parentKeyInd = i
}

func (n *internalNode) Parent() *internalNode {
	return n.parent
}

func (n *internalNode) SetParent(p *internalNode) {
	n.parent = p
}

func (n *internalNode) Link() *link {
	return n.link
}

func (n *internalNode) Count() int {
	return n.count
}

func (n *internalNode) searchKeyIndex(key Entry) (int, bool) {
	return searchKeyIndex(n.keys, n.count-1, key)
}

func (n *internalNode) splitLeft(left *internalNode, leftCh vertex, rightCh vertex, key Entry, insertInd int) (splitKey Entry) {
	order := n.count
	splitInd := cut(order)

	// copy 1st half. from sub[0] to sub[split-1]
	for i, j := 0, 0; i < splitInd; i, j = i+1, j+1 {
		if j != insertInd {
			p := n.nodes[i]
			p.SetParent(left)
			p.SetParentKeyInd(j - 1)
			left.nodes[j] = p
		} else {
			p := leftCh
			p.SetParent(left)
			p.SetParentKeyInd(j - 1)
			left.nodes[j] = p
			q := rightCh
			q.SetParent(left)
			q.SetParentKeyInd(j)
			left.nodes[j+1] = q
			j++
		}
	}
	left.count = splitInd

	// copy 2nd half. from keys[0] to keys[split-2]
	var parentKeyInd int
	for i, j := 0, 0; i < splitInd-1; j++ {
		if j == insertInd {
			left.keys[j] = key
		} else {
			left.keys[j] = n.keys[i]
			i++
		}
		parentKeyInd = j
	}

	if insertInd != splitInd-1 {
		n.nodes[0] = n.nodes[splitInd-1]
		splitKey = n.keys[splitInd-2]
	} else {
		left.keys[insertInd] = key
		p := leftCh
		p.SetParent(left)
		p.SetParentKeyInd(parentKeyInd)
		left.nodes[insertInd] = p
		n.nodes[0] = rightCh
		splitKey = key
	}

	n.nodes[0].SetParent(n)
	n.nodes[0].SetParentKeyInd(-1)

	// left shift for right node from split-1 to children-1
	var i, j int
	for i, j = splitInd-1, 0; i < order-1; i, j = i+1, j+1 {
		n.keys[j] = n.keys[i]
		p := n.nodes[i+1]
		p.SetParent(n)
		p.SetParentKeyInd(j)
		n.nodes[j+1] = p
	}

	n.nodes[j] = n.nodes[i]
	n.count = j + 1

	return
}

func (n *internalNode) splitRight1(right *internalNode, leftCh vertex, rightCh vertex, key Entry, insertInd int) (splitKey Entry) {
	order := n.count
	splitInd := cut(order)
	splitKey = n.keys[splitInd-1]
	n.count = splitInd

	// set up right's first two child nodes
	right.keys[0] = key

	leftCh.SetParent(right)
	leftCh.SetParentKeyInd(-1)
	right.nodes[0] = leftCh

	rightCh.SetParent(right)
	rightCh.SetParentKeyInd(0)
	right.nodes[1] = rightCh

	var i, j int
	for i, j = splitInd, 1; i < order-1; i, j = i+1, j+1 {
		right.keys[j] = n.keys[i]
		p := n.nodes[i+1]
		p.SetParent(right)
		p.SetParentKeyInd(j)
		right.nodes[j+1] = p
	}
	right.count = j + 1
	return
}

func (n *internalNode) splitRight2(right *internalNode, leftCh vertex, rightCh vertex, key Entry, insertInd int) (splitKey Entry) {
	order := n.count
	splitInd := cut(order)
	n.count = splitInd + 1
	splitKey = n.keys[splitInd]

	// set up right node's first child
	child := n.nodes[splitInd+1]
	child.SetParent(right)
	child.SetParentKeyInd(-1)
	right.nodes[0] = child

	var i, j int
	// copy from keys[splitInd+1] to keys[order-1]
	for i, j = splitInd+1, 0; i < order-1; j++ {
		if j == insertInd-splitInd-1 {
			continue
		}
		right.keys[j] = n.keys[i]
		p := n.nodes[i+1]
		p.SetParent(right)
		p.SetParentKeyInd(j)
		right.nodes[j+1] = p
		i++
	}

	// reserve hole for insertion
	if j > insertInd-splitInd-1 {
		right.count = j + 1
	} else {
		if j != insertInd-splitInd-1 {
			err := fmt.Errorf("ERROR: no hole to insert. key: %d. j: %d. insertInd: %d. splitInd: %d. LeafNode: %+v", key, j, insertInd, splitInd, n)
			panic(err)
		}
		right.count = j + 2
	}

	// insert new key and child node
	j = insertInd - splitInd - 1
	right.keys[j] = key

	leftCh.SetParent(right)
	leftCh.SetParentKeyInd(j - 1)
	right.nodes[j] = leftCh

	rightCh.SetParent(right)
	rightCh.SetParentKeyInd(j)
	right.nodes[j+1] = rightCh

	return
}

func (n *internalNode) addLeftLink(sib *internalNode) {
	li := n.link.List
	sib.link = &link{
		Element: li.InsertBefore(sib, n.link.Element),
		List:    li,
	}
}

func (n *internalNode) addRightLink(sib *internalNode) {
	li := n.link.List
	sib.link = &link{
		Element: li.InsertAfter(sib, n.link.Element),
		List:    li,
	}
}

func (n *internalNode) removeLink(sib *internalNode) {
	n.link.List.Remove(sib.link.Element)
	sib.link = nil
}

// insert is only used when the node isn't already full. To insert when the node
// is full, use splitInsert.
func (n *internalNode) insert(leftCh vertex, rightCh vertex, key Entry, insertInd int) {
	var i int
	for i = n.count - 1; i > insertInd; i-- {
		n.keys[i] = n.keys[i-1]
		n.nodes[i+1] = n.nodes[i]
		n.nodes[i+1].SetParentKeyInd(i)
	}
	n.keys[i] = key
	n.nodes[i] = leftCh
	n.nodes[i+1] = rightCh
	n.count++

	leftCh.SetParent(n)
	leftCh.SetParentKeyInd(i - 1)
	rightCh.SetParent(n)
	rightCh.SetParentKeyInd(i)
}

func (n *internalNode) remove(removeInd int) {
	if n.count < 2 {
		panic("Node already at minimum number of children")
	}
	for i := removeInd; i < n.count-2; i++ {
		n.keys[i] = n.keys[i+1]
		n.nodes[i+1] = n.nodes[i+2]
		n.nodes[i+1].SetParentKeyInd(i)
	}
	n.count--
}

func (n *internalNode) whichBorrowee() siblingDirection {
	leftSib := n.prev()
	rightSib := n.next()
	return whichBorrowee(leftSib, rightSib, n.parent, n.parentKeyInd)
}

func (n *internalNode) shiftFromLeft(left *internalNode, parentKeyInd int, removeInd int) {
	for i := removeInd; i > 0; i-- {
		n.keys[i] = n.keys[i-1]
	}
	for i := removeInd + 1; i > 0; i-- {
		n.nodes[i] = n.nodes[i-1]
		n.nodes[i].SetParentKeyInd(i - 1)
	}
	// parent key right rotation
	n.keys[0] = n.parent.keys[parentKeyInd]
	n.parent.keys[parentKeyInd] = left.keys[left.count-2]
	// borrow last child from left sibling
	child := left.nodes[left.count-1]
	child.SetParent(n)
	child.SetParentKeyInd(-1)
	n.nodes[0] = child
	left.nodes[left.count-1] = nil // "disown" child
	left.count--
}

func (n *internalNode) mergeIntoLeft(left *internalNode, parentKeyInd int, removeInd int) {
	// move parent key down
	left.keys[left.count-1] = n.parent.keys[parentKeyInd]
	// merge into left sibling
	for i, j := left.count, 0; j < n.count-1; j++ {
		if j != removeInd {
			left.keys[i] = n.keys[j]
			i++
		}
	}
	ind := left.count
	for j := 0; j < n.count; j++ {
		if j == removeInd+1 {
			continue
		}
		child := n.nodes[j]
		child.SetParent(left)
		child.SetParentKeyInd(ind - 1)
		left.nodes[ind] = child
		ind++
	}
	left.count = ind
}

func (n *internalNode) shiftFromRight(right *internalNode, parentKeyInd int) {
	// parent key left rotation
	n.keys[n.count-1] = n.parent.keys[parentKeyInd]
	n.parent.keys[parentKeyInd] = right.keys[0]
	// borrow first child node from right sibling
	child := right.nodes[0]
	child.SetParent(n)
	child.SetParentKeyInd(n.count - 1)
	n.nodes[n.count] = child
	n.count++
	// left shift into right sibling
	for i := 0; i < right.count-2; i++ {
		right.keys[i] = right.keys[i+1]
	}
	for i := 0; i < right.count-1; i++ {
		child := right.nodes[i+1]
		child.SetParentKeyInd(i - 1)
		right.nodes[i] = child
	}
	right.count--
}

func (n *internalNode) mergeFromRight(right *internalNode, parentKeyInd int) {
	// move parent key down
	n.keys[n.count-1] = n.parent.keys[parentKeyInd]
	n.count++
	// merge from right sibling
	for i, j := n.count-1, 0; j < right.count-1; i, j = i+1, j+1 {
		n.keys[i] = right.keys[j]
	}
	ind := n.count - 1
	for j := 0; j < right.count; j++ {
		rightCh := right.nodes[j]
		rightCh.SetParent(n)
		rightCh.SetParentKeyInd(ind - 1)
		n.nodes[ind] = rightCh
		ind++
	}
	n.count = ind
}

func (n *internalNode) prev() *internalNode {
	left := n.link.Element.Prev()
	if left == nil {
		return nil
	}
	sib, _ := left.Value.(*internalNode)
	return sib
}

func (n *internalNode) next() *internalNode {
	right := n.link.Element.Next()
	if right == nil {
		return nil
	}
	sib, _ := right.Value.(*internalNode)
	return sib
}

// A leafNode contains the indexed data for the tree. The keys field holds this
// data in each Entry. The parentKeyInd field represents its place relative to
// any sibling leafNodes with respect to its parent. A value of -1 means it is
// the left-most child of its parent. The length of keys does not change;
// instead Entry values start out as nil and are replaced, swapped, copied as
// needed. The relevant, or active, values will be in the range [0, count].
type leafNode struct {
	parentKeyInd int
	parent       *internalNode
	link         *link
	count        int // number of active values in 'keys' field
	keys         []Entry
}

func newLeafNode(numKeys int) *leafNode {
	var node leafNode
	node.parentKeyInd = -1
	// The link field is assigned when added to Tree's list
	node.keys = make([]Entry, numKeys)
	return &node
}

func (n *leafNode) ParentKeyInd() int {
	return n.parentKeyInd
}

func (n *leafNode) SetParentKeyInd(i int) {
	n.parentKeyInd = i
}

func (n *leafNode) Parent() *internalNode {
	return n.parent
}

func (n *leafNode) SetParent(p *internalNode) {
	n.parent = p
}

func (n *leafNode) Link() *link {
	return n.link
}

func (n *leafNode) Count() int {
	return n.count
}

func (n *leafNode) searchKeyIndex(key Entry) (int, bool) {
	return searchKeyIndex(n.keys, n.count, key)
}

func (n *leafNode) insert(key Entry, insertInd int) {
	var i int

	cmp := n.keys[insertInd]
	if cmp != nil && !key.Less(cmp) && !cmp.Less(key) {
		i = insertInd
	} else {
		for i = n.count; i > insertInd; i-- {
			n.keys[i] = n.keys[i-1]
		}
		n.count++
	}

	n.keys[i] = key
}

func (n *leafNode) splitLeft(left *leafNode, key Entry, insertInd int) {
	splitInd := cut(n.count)

	// replicate from 0 to keys[split-2]
	var i, j int
	for ; i < splitInd-1; j++ {
		if j == insertInd {
			left.keys[j] = key
		} else {
			left.keys[j] = n.keys[i]
			i++
		}
	}

	if j == insertInd {
		left.keys[j] = key
		j++
	}

	left.count = j

	// left shift for right node
	for j = 0; i < n.count; i, j = i+1, j+1 {
		n.keys[j] = n.keys[i]
	}

	n.count = j
}

func (n *leafNode) splitRight(right *leafNode, key Entry, insertInd int) {
	splitInd := cut(n.count)

	// replicate from keys[split]
	var i, j int
	for i, j = splitInd, 0; i < n.count; j++ {
		if j == insertInd-splitInd {
			continue
		}
		right.keys[j] = n.keys[i]
		i++
	}

	// reserve hole for insertion
	if j > insertInd-splitInd {
		right.count = j
	} else {
		if j != insertInd-splitInd {
			err := fmt.Errorf("ERROR: no hole to insert. key: %d. j: %d. insertInd: %d. splitInd: %d. LeafNode: %+v", key, j, insertInd, splitInd, n)
			panic(err)
		}
		right.count = j + 1
	}

	j = insertInd - splitInd
	right.keys[j] = key
	n.count = splitInd
}

func (n *leafNode) addLeftLink(sib *leafNode) {
	li := n.link.List
	sib.link = &link{
		Element: li.InsertBefore(sib, n.link.Element),
		List:    li,
	}
}

func (n *leafNode) addRightLink(sib *leafNode) {
	li := n.link.List
	sib.link = &link{
		Element: li.InsertAfter(sib, n.link.Element),
		List:    li,
	}
}

func (n *leafNode) removeLink(sib *leafNode) {
	n.link.List.Remove(sib.link.Element)
	sib.link = nil
}

func (n *leafNode) remove(removeInd int) {
	for i := removeInd; i < n.count-1; i++ {
		n.keys[i] = n.keys[i+1]
	}
	n.count--
}

func (n *leafNode) whichBorrowee() siblingDirection {
	leftSib := n.prev()
	rightSib := n.next()
	return whichBorrowee(leftSib, rightSib, n.parent, n.parentKeyInd)
}

func (n *leafNode) shiftFromLeft(left *leafNode, parentKeyInd int, removeInd int) {
	// right shift in leaf node
	for i := removeInd; i > 0; i-- {
		n.keys[i] = n.keys[i-1]
	}
	// borrow last element from left sibling
	n.keys[0] = left.keys[left.count-1]
	left.count--
	n.parent.keys[parentKeyInd] = n.keys[0]
}

func (n *leafNode) mergeIntoLeft(left *leafNode, removeInd int) {
	i := left.count
	// merge into left sibling
	for j := 0; j < n.count; j++ {
		if j == removeInd {
			continue
		}
		left.keys[i] = n.keys[j]
		i++
	}
	left.count = i
}

func (n *leafNode) shiftFromRight(right *leafNode, parentKeyInd int) {
	// borrow first element from right sibling
	n.keys[n.count] = right.keys[0]
	n.count++
	// left shift in right sibling
	for i := 0; i < right.count-1; i++ {
		right.keys[i] = right.keys[i+1]
	}
	right.count--

	n.parent.keys[parentKeyInd] = right.keys[0]
}

func (n *leafNode) mergeFromRight(right *leafNode) {
	i := n.count
	// merge from right sibling
	for j := 0; j < right.count; i, j = i+1, j+1 {
		n.keys[i] = right.keys[j]
	}
	n.count = i
}

func (n *leafNode) prev() *leafNode {
	leftLink := n.link.Element.Prev()
	if leftLink == nil {
		return nil
	}
	sib, _ := leftLink.Value.(*leafNode)
	return sib
}

func (n *leafNode) next() *leafNode {
	rightLink := n.link.Element.Next()
	if rightLink == nil {
		return nil
	}
	sib, _ := rightLink.Value.(*leafNode)
	return sib
}

// siblingDirection is a helper type to signal which sibling a vertex should
// borrow from during key deletion.
type siblingDirection int

const (
	siblingToLeft  = siblingDirection(-1)
	siblingToRight = siblingDirection(+1)
)

// whichBorrowee returns the direction of the sibling node to borrow from while
// deleting a key from the tree.
func whichBorrowee(leftSib, rightSib vertex, parent *internalNode, parentKeyInd int) siblingDirection {
	if parentKeyInd == -1 {
		// no left sibling available
		return siblingToRight
	}
	if parentKeyInd == parent.count-2 {
		// no right sibling available
		return siblingToLeft
	}
	if leftSib.Count() >= rightSib.Count() {
		return siblingToLeft
	}
	return siblingToRight
}

func isLeaf(v *vertex) bool {
	switch val := (*v).(type) {
	case *leafNode:
		return true
	case *internalNode:
		return false
	default:
		err := fmt.Sprintf("Unexpected value %v of type %T", val, val)
		panic(err)
	}
}

// searchKeyIndex finds the position of  target in keys by performing a binary
// search; keys must be sorted if it isn't already. When keys is part of a
// LeafNode, then max should be the number of entries in the LeafNode. When keys
// is part of an InternalNode, then max should be the number of children in the
// InternalNode minus 1. If target is not found, then found is false and the
// return value ind is what the position of target would be if it were inserted.
func searchKeyIndex(keys []Entry, max int, target Entry) (ind int, found bool) {
	ind = sort.Search(max, func(i int) bool {
		return target.Less(keys[i])
	})

	// if ind > 0 && keys[ind-1] >= target {
	if ind > 0 && !keys[ind-1].Less(target) {
		ind--
		found = true
		return
	}

	return
}

// cut returns the correct index to a split a Node in two.
func cut(n int) int {
	return (n + 1) / 2
}

// A link is used in a vertex to reference left and right siblings. It also
// references its parent List to help facilitate key deletion and range
// searches.
type link struct {
	*list.Element
	*list.List
}

// Entry is a comparable item to store in the tree.
type Entry interface {
	// Less tests whether or not the item is less than the input.
	//
	// This must provide a strict weak ordering.
	// If !a.Less(b) && !b.Less(a), we treat this to mean a == b (i.e. we can
	// only hold one of either a or b in the tree).
	Less(than Entry) bool
}

// Int implements the Entry interface for integers.
type Int int

// Less returns true if int(a) < int(b).
func (a Int) Less(b Entry) bool {
	return a < b.(Int)
}
