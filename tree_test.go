package bpt

import (
	"fmt"
	"math/rand"
	"sort"
	"testing"
	"time"
)

func init() {
	seed := time.Now().UnixNano()
	fmt.Printf("rand seed %d\n", seed)
	rand.Seed(seed)
}

func TestTree(t *testing.T) {
	t.Run("Put, Get", func(t *testing.T) {
		tests := []struct {
			order int
		}{
			{order: 4},
		}

		for ind, test := range tests {
			tree, err := NewTree(test.order)
			if err != nil {
				panic(err)
			}

			inputKeys := []Int{24, 72, 1, 39, 53, 63, 90, 88, 15, 10, 44, 68, 74}
			for _, num := range inputKeys {
				tree.Put(num)
			}
			if tree.Len() != len(inputKeys) {
				t.Errorf("Test %d. Wrong length, got %d, expected %d", ind, tree.Len(), len(inputKeys))
			}

			// expect to find each number in the tree
			for j, num := range inputKeys {
				out := tree.Get(num)
				if out != num {
					t.Errorf("Test %d, %d. Did not find num. Got %v, expected %v", ind, j, out, num)
				}
			}

			// expect not to find these
			shouldBeMissing := []Int{0, 50, 100}
			for j, num := range shouldBeMissing {
				out := tree.Get(num)
				if out != nil {
					t.Errorf("Test %d, %d. Expected not to find %d", ind, j, num)
				}
			}
			if ok := testTreeNodesFromRoot(t, tree.root); !ok {
				dumpTreeInTestContext(t, tree)
			}
		}
	})

	t.Run("Max Min", func(t *testing.T) {
		tree, err := NewTree(4)
		if err != nil {
			panic(err)
		}

		if tree.Max() != nil {
			t.Errorf("Wrong value for Max. Got %d, expected %v", tree.Max(), nil)
		}
		if tree.Min() != nil {
			t.Errorf("Wrong value for Min. Got %d, expected %v", tree.Min(), nil)
		}

		for i := 1; i <= 10; i++ {
			tree.Put(Int(i))
		}
		var exp Int
		exp = Int(10)
		if tree.Max() != exp {
			t.Errorf("Wrong value for Max. Got %d, expected %d", tree.Max(), exp)
		}
		exp = Int(1)
		if tree.Min() != exp {
			t.Errorf("Wrong value for Min. Got %d, expected %d", tree.Min(), exp)
		}
	})

	t.Run("updating values", func(t *testing.T) {
		tests := []struct {
			treeOrder int
			inputs    []Entry
			updates   []Entry
		}{
			{
				treeOrder: 4,
				inputs:    []Entry{testRecord{Int: 5, val: false}, testRecord{Int: 25, val: false}, testRecord{Int: 15, val: false}},
				updates: []Entry{
					testRecord{Int: 5, val: true},
					testRecord{Int: 25, val: true},
					testRecord{Int: 15, val: true},
				},
			},
			{
				treeOrder: 4,
				inputs: []Entry{
					testRecord{Int: 5, val: false}, testRecord{Int: 25, val: false}, testRecord{Int: 15, val: false},
					testRecord{Int: 35, val: false}, testRecord{Int: 10, val: false}, testRecord{Int: 40, val: false},
					testRecord{Int: 20, val: false}, testRecord{Int: 30, val: false}, testRecord{Int: 12, val: false},
					testRecord{Int: 42, val: false}, testRecord{Int: 22, val: false}, testRecord{Int: 32, val: false},
				},
				updates: []Entry{
					testRecord{Int: 5, val: true}, testRecord{Int: 25, val: true}, testRecord{Int: 15, val: true},
					testRecord{Int: 35, val: true}, testRecord{Int: 10, val: true}, testRecord{Int: 40, val: true},
					testRecord{Int: 20, val: true}, testRecord{Int: 30, val: true}, testRecord{Int: 12, val: true},
					testRecord{Int: 42, val: true}, testRecord{Int: 22, val: true}, testRecord{Int: 32, val: true},
				},
			},
		}

		for i, test := range tests {
			tree, err := NewTree(test.treeOrder)
			if err != nil {
				panic(err)
			}

			for j, entry := range test.updates {
				for _, input := range test.inputs {
					tree.Put(input)
				}
				tree.Put(entry)

				upd := entry.(testRecord)
				out := tree.Get(upd).(testRecord)
				if out.val != upd.val {
					t.Errorf("Test %d %d. Did not find num. Got %t, expected %t", i, j, out.val, upd.val)
				}
			}
			if tree.Len() != len(test.inputs) {
				t.Errorf("Test %d. Wrong length, got %d, expected %d", i, tree.Len(), len(test.inputs))
			}
			if ok := testTreeNodesFromRoot(t, tree.root); !ok {
				dumpTreeInTestContext(t, tree)
			}
		}
	})

	t.Run("splitting internalNode", func(t *testing.T) {
		commonInputs := []Int{
			10, 14, 20, 26, 32,
			38, 44, 48, 54, 60,
			66, 72, 80, 84,
		}

		// calls internalNode.splitLeft
		t.Run("insertIndex<splitIndex", func(t *testing.T) {
			tree, err := NewTree(4)
			if err != nil {
				panic(err)
			}
			inputs := make([]Int, len(commonInputs))
			copy(inputs, commonInputs)
			inputs = append(inputs, 46, 50)

			for i, num := range inputs {
				tree.Put(num)
				if ok := testTreeNodesFromRoot(t, tree.root); !ok {
					dumpTreeInTestContext(t, tree)
					return
				}
				if tree.Len() != i+1 {
					t.Errorf("Wrong length, got %d, expected %d", tree.Len(), i+1)
					return
				}

				// check internalNode linked lists are correct
				node, ok := tree.root.(*internalNode)
				if !ok {
					continue
				}
				if ok := testNodeSiblings(t, node.nodes[:node.count]); !ok {
					t.Errorf("internalNode.list wrong after putting %d", num)
					dumpTreeInTestContext(t, tree)
					return
				}
			}
		})

		// calls internalNode.splitRight1
		t.Run("insertIndex==splitIndex", func(t *testing.T) {
			tree, err := NewTree(4)
			if err != nil {
				panic(err)
			}
			inputs := make([]Int, len(commonInputs))
			copy(inputs, commonInputs)
			inputs = append(inputs, 75, 78)

			for i, num := range inputs {
				tree.Put(num)
				if ok := testTreeNodesFromRoot(t, tree.root); !ok {
					dumpTreeInTestContext(t, tree)
					return
				}
				if tree.Len() != i+1 {
					t.Errorf("Wrong length, got %d, expected %d", tree.Len(), i+1)
					return
				}

				// check internalNode linked lists are correct
				node, ok := tree.root.(*internalNode)
				if !ok {
					continue
				}
				if ok := testNodeSiblings(t, node.nodes[:node.count]); !ok {
					t.Errorf("internalNode.list wrong after putting %d", num)
					dumpTreeInTestContext(t, tree)
					return
				}
			}
		})

		// calls internalNode.splitRight2
		t.Run("insertIndex>splitIndex", func(t *testing.T) {
			tree, err := NewTree(4)
			if err != nil {
				panic(err)
			}
			inputs := make([]Int, len(commonInputs))
			copy(inputs, commonInputs)
			inputs = append(inputs, 82, 90)

			for i, num := range inputs {
				tree.Put(num)
				if ok := testTreeNodesFromRoot(t, tree.root); !ok {
					dumpTreeInTestContext(t, tree)
					return
				}
				if tree.Len() != i+1 {
					t.Errorf("Wrong length, got %d, expected %d", tree.Len(), i+1)
					return
				}

				// check internalNode linked lists are correct
				node, ok := tree.root.(*internalNode)
				if !ok {
					continue
				}
				if ok := testNodeSiblings(t, node.nodes[:node.count]); !ok {
					t.Errorf("internalNode.list wrong after putting %d", num)
					dumpTreeInTestContext(t, tree)
					return
				}
			}
		})
	})

	t.Run("corner case", func(t *testing.T) {
		inputs := []Int{7, 11, 3, 8, 12, 2, 15, 13, 4, 16, 6, 10, 1, 5, 9}
		tree, err := NewTree(4)
		if err != nil {
			panic(err)
		}

		for _, num := range inputs {
			tree.Put(num)
			if ok := testTreeNodesFromRoot(t, tree.root); !ok {
				dumpTreeInTestContext(t, tree)
				return
			}

			// check internalNode linked lists are correct
			node, ok := tree.root.(*internalNode)
			if !ok {
				continue
			}
			if ok := testNodeSiblings(t, node.nodes[:node.count]); !ok {
				t.Errorf("internalNode.list wrong after putting %d", num)
				dumpTreeInTestContext(t, tree)
				return
			}
		}
		if tree.Len() != len(inputs) {
			t.Errorf("Wrong length, got %d, expected %d", tree.Len(), len(inputs))
			return
		}
	})
}

func TestTreeDel(t *testing.T) {
	t.Run("randomized tree size and inputs", func(t *testing.T) {
		order := rand.Intn(60) + 5 // [5, 64)
		tree, err := NewTree(order)
		if err != nil {
			panic(err)
		}
		numKeys := rand.Intn(order*order) + order*2
		t.Logf("Tree.order %d, Tree.entries %d, numKeys %d\n", tree.order, tree.entries, numKeys)
		inputs := make([]Int, numKeys)
		for i := 0; i < numKeys; i++ {
			inputs[i] = Int(i + 1)
		}
		// track keys in Tree, so we know which to remove later
		inserted := make(map[Int]bool)
		rand.Shuffle(numKeys, func(i, j int) {
			inputs[i], inputs[j] = inputs[j], inputs[i]
		})
		for i := 0; i < len(inputs); i++ {
			num := inputs[i]
			tree.Put(num)
			inserted[num] = true
			if ok := testTreeNodesFromRoot(t, tree.root); !ok {
				t.Errorf("Problem after putting %v", num)
				dumpTreeInTestContext(t, tree)
				return
			}

			// check internalNode linked lists are correct
			node, ok := tree.root.(*internalNode)
			if !ok {
				continue
			}
			if ok := testNodeSiblings(t, node.nodes[:node.count]); !ok {
				t.Errorf("internalNode.list wrong")
				dumpTreeInTestContext(t, tree)
				return
			}
		}
		if tree.Len() != len(inputs) {
			t.Errorf("Wrong length, got %d, expected %d", tree.Len(), len(inputs))
			return
		}
		toRemove := make(map[Int]bool)
		r := rand.Intn(len(inputs))
		for key := range inserted { // random enough, key iteration order not guaranteed
			if r == 0 {
				break
			}
			toRemove[key] = true
			r--
		}
		wasRemoved := make(map[Int]bool)
		numRemoved := 0
		for key := range toRemove {
			tree.Del(key)
			numRemoved++
			if ok := testTreeNodesFromRoot(t, tree.root); !ok {
				t.Errorf("Problem after deleting %v", key)
				dumpTreeInTestContext(t, tree)
				return
			}
			if tree.Len() != len(inputs)-numRemoved {
				t.Errorf("Wrong length, got %d, expected %d", tree.Len(), len(inputs)-numRemoved)
				return
			}
			if out := tree.Get(key); out == nil {
				wasRemoved[key] = true
			}
			// removing one key should not remove other keys
			for _, num := range inputs {
				_, rm := wasRemoved[num]
				out := tree.Get(num)
				if rm && out != nil {
					t.Errorf("Did not expect to find %d. Got %v, expected %v", num, out, nil)
					dumpTreeInTestContext(t, tree)
					return
				}
				if !rm && out == nil {
					t.Errorf("Expected to find %d", num)
					dumpTreeInTestContext(t, tree)
					return
				}
			}
		}
	})

	t.Run("fixed", func(t *testing.T) {
		const numKeys = 27
		inputs := make([]Int, 0)
		for i := Int(5); len(inputs) < numKeys; i += 5 {
			inputs = append(inputs, i)
		}
		tests := []struct {
			toRemove []Int
		}{
			// calls internalNode.shiftFromRight
			{toRemove: []Int{90, 85, 80, 75, 70, 65}},
			// calls internalNode.mergeFromRight
			{toRemove: []Int{5, 10, 15, 20, 25, 30}},
			// calls internalNode.mergeIntoLeft
			{toRemove: []Int{135, 130, 125, 120, 115, 110, 105, 100, 95}},
			// remove root node
			{toRemove: inputs},
		}
		for ind, test := range tests {
			tree, err := NewTree(4)
			if err != nil {
				panic(err)
			}
			for _, ent := range inputs {
				tree.Put(ent)

				// check internalNode linked lists are correct
				node, ok := tree.root.(*internalNode)
				if !ok {
					continue
				}
				if ok := testNodeSiblings(t, node.nodes[:node.count]); !ok {
					t.Errorf("internalNode.list wrong")
					return
				}
			}
			if tree.Len() != numKeys {
				t.Errorf("Test %d. Wrong length, got %d, expected %d", ind, tree.Len(), numKeys)
				return
			}
			// track removed keys so we can test unwanted side-effects
			wasRemoved := make(map[Int]bool)
			numRemoved := 0
			for _, num := range test.toRemove {
				wasInserted := tree.Get(num) != nil
				out := tree.Del(num)
				if out != num {
					t.Errorf("Test %d. Removing %d should return key. Got %v, expected %v", ind, num, out, num)
				}
				if wasInserted && out == num {
					wasRemoved[num] = true
					numRemoved++
				}
				if wasRemoved[num] && tree.Len() != numKeys-numRemoved {
					t.Errorf("Wrong length, got %d, expected %d", tree.Len(), numKeys-numRemoved)
				}

				// removing one key should not remove other keys
				for _, ent := range inputs {
					_, rm := wasRemoved[ent]
					out := tree.Get(ent)
					if !rm && out == nil {
						t.Errorf("Expected to find %d", ent)
						t.Logf("%#v\n", wasRemoved)
						dumpTreeInTestContext(t, tree)
						return
					} else if rm && out != nil {
						t.Errorf("Test %d, did not expect to find %d. Got %d, expected %v", ind, ent, out, nil)
						dumpTreeInTestContext(t, tree)
						return
					}
				}
			}
		}
	})

	t.Run("not found", func(t *testing.T) {
		tree, err := NewTree(4)
		if err != nil {
			panic(err)
		}
		const numKeys = 10

		for i := 1; i <= numKeys; i++ {
			tree.Put(Int(i))
		}

		if out := tree.Del(Int(20)); out != nil {
			t.Errorf("Should not return value if it's not in Tree in first place. Got %v, expected %v", out, nil)
		}
		if tree.Len() != numKeys {
			t.Errorf("Tree length should remain unchanged. Got %d, expected %d", tree.Len(), numKeys)
		}

		if out := tree.Del(nil); out != nil {
			t.Errorf("Should not return value if it's not in Tree in first place. Got %v, expected %v", out, nil)
		}
		if tree.Len() != numKeys {
			t.Errorf("Tree length should remain unchanged. Got %d, expected %d", tree.Len(), numKeys)
		}
	})

	t.Run("Max Min", func(t *testing.T) {
		tree, err := NewTree(4)
		if err != nil {
			panic(err)
		}

		if out := tree.DelMax(); out != nil {
			t.Errorf("Wrong value for DelMax. Got %d, expected %v", out, nil)
		}
		if out := tree.DelMin(); out != nil {
			t.Errorf("Wrong value for DelMin. Got %d, expected %v", out, nil)
		}

		for i := 1; i <= 10; i++ {
			tree.Put(Int(i))
		}
		var exp Int
		exp = Int(10)
		if out := tree.DelMax(); out != exp {
			t.Errorf("Wrong value for DelMax. Got %d, expected %d", out, exp)
		}
		if out := tree.Get(exp); out != nil {
			t.Errorf("Did not expect to find %v after DelMax. Got %d, expected %v", exp, out, nil)
		}
		exp = Int(1)
		if out := tree.DelMin(); out != exp {
			t.Errorf("Wrong value for DelMin. Got %d, expected %d", out, exp)
		}
		if out := tree.Get(exp); out != nil {
			t.Errorf("Did not expect to find %v after DelMin. Got %d, expected %v", exp, out, nil)
		}
	})
}

func TestTreeAscendDescend(t *testing.T) {
	testTreeRange := func(t *testing.T, out []Entry, expLen int, expLo, expHi Int) bool {
		t.Helper()
		ok := true
		n := len(out)
		if n != expLen {
			t.Errorf("Wrong length. Got %d, expected %d", n, expLen)
			ok = false
		}
		if n < 1 {
			return ok
		}
		if out[0].Less(expLo) || expLo.Less(out[0]) {
			t.Errorf("Wrong value for out[0]. Got %d, expected %d", out[0], expLo)
			ok = false
		}
		if out[n-1].Less(expHi) || expHi.Less(out[n-1]) {
			t.Errorf("Wrong value for out[n-1]. Got %d, expected %d", out[n-1], expHi)
			ok = false
		}
		return ok
	}

	const (
		intRangeLo   = 4
		intRangeHi   = 32
		intRangeStep = 2
		treeOrder    = 4
	)

	type testCase struct {
		lo     Entry
		hi     Entry
		expLen int
		expLo  Int
		expHi  Int
	}

	t.Run("Ascend", func(t *testing.T) {
		tests := []testCase{
			{expLen: 15, expLo: Int(4), expHi: Int(32)},
		}

		for i, test := range tests {
			inputs := makeIntRangeStep(intRangeLo, intRangeHi, intRangeStep)
			tree, _ := NewTree(treeOrder)
			for _, key := range inputs {
				tree.Put(key)
			}
			out := make([]Entry, 0)
			tree.Ascend(func(ent Entry) bool {
				out = append(out, ent)
				return true
			})
			if ok := testTreeRange(t, out, test.expLen, test.expLo, test.expHi); !ok {
				t.Errorf("Failure, test %d", i)
			}
		}
	})

	t.Run("Descend", func(t *testing.T) {
		tests := []testCase{
			{expLen: 15, expLo: Int(4), expHi: Int(32)},
		}

		for i, test := range tests {
			inputs := makeIntRangeStep(intRangeLo, intRangeHi, intRangeStep)
			tree, _ := NewTree(treeOrder)
			for _, key := range inputs {
				tree.Put(key)
			}
			out := make([]Entry, 0)
			tree.Descend(func(ent Entry) bool {
				out = append(out, ent)
				return true
			})
			if ok := testTreeRange(t, out, test.expLen, test.expHi, test.expLo); !ok {
				t.Errorf("Failure, test %d", i)
			}
		}
	})

	t.Run("AscendGreaterOrEqual", func(t *testing.T) {
		tests := []testCase{
			{lo: Int(-5), expLen: 15, expLo: Int(4), expHi: Int(32)},
			{lo: Int(7), expLen: 13, expLo: Int(8), expHi: Int(32)},
			{lo: Int(8), expLen: 13, expLo: Int(8), expHi: Int(32)},
			{lo: Int(32), expLen: 1, expLo: Int(32), expHi: Int(32)},
			{lo: Int(33), expLen: 0},
		}
		for i, test := range tests {
			inputs := makeIntRangeStep(intRangeLo, intRangeHi, intRangeStep)
			tree, _ := NewTree(treeOrder)
			for _, key := range inputs {
				tree.Put(key)
			}
			pivot := test.lo.(Int)
			out := make([]Entry, 0)
			tree.AscendGreaterOrEqual(pivot, func(ent Entry) bool {
				out = append(out, ent)
				return true
			})
			if ok := testTreeRange(t, out, test.expLen, test.expLo, test.expHi); !ok {
				t.Errorf("Failure, test %d", i)
			}
		}
	})

	t.Run("DescendGreaterThan", func(t *testing.T) {
		tests := []testCase{
			{lo: Int(3), expLen: 15, expLo: Int(4), expHi: Int(32)},
			{lo: Int(7), expLen: 13, expLo: Int(8), expHi: Int(32)},
			{lo: Int(8), expLen: 12, expLo: Int(10), expHi: Int(32)},
			{lo: Int(32), expLen: 0},
		}
		for i, test := range tests {
			inputs := makeIntRangeStep(intRangeLo, intRangeHi, intRangeStep)
			tree, _ := NewTree(treeOrder)
			for _, key := range inputs {
				tree.Put(key)
			}
			pivot := test.lo.(Int)
			out := make([]Entry, 0)
			tree.DescendGreaterThan(pivot, func(ent Entry) bool {
				out = append(out, ent)
				return true
			})
			if ok := testTreeRange(t, out, test.expLen, test.expHi, test.expLo); !ok {
				t.Errorf("Failure, test %d", i)
			}
		}
	})

	t.Run("AscendLessThan", func(t *testing.T) {
		tests := []testCase{
			{hi: Int(4), expLen: 0},
			{hi: Int(10), expLen: 3, expLo: Int(4), expHi: Int(8)},
			{hi: Int(11), expLen: 4, expLo: Int(4), expHi: Int(10)},
			{hi: Int(33), expLen: 15, expLo: Int(4), expHi: Int(32)},
		}
		for i, test := range tests {
			inputs := makeIntRangeStep(intRangeLo, intRangeHi, intRangeStep)
			tree, _ := NewTree(treeOrder)
			for _, key := range inputs {
				tree.Put(key)
			}
			pivot := test.hi.(Int)
			out := make([]Entry, 0)
			tree.AscendLessThan(pivot, func(ent Entry) bool {
				out = append(out, ent)
				return true
			})
			if ok := testTreeRange(t, out, test.expLen, test.expLo, test.expHi); !ok {
				t.Errorf("Failure, test %d", i)
			}
		}
	})

	t.Run("DescendLessOrEqual", func(t *testing.T) {
		tests := []testCase{
			{hi: Int(4), expLen: 1, expLo: Int(4), expHi: Int(4)},
			{hi: Int(10), expLen: 4, expLo: Int(4), expHi: Int(10)},
			{hi: Int(11), expLen: 4, expLo: Int(4), expHi: Int(10)},
			{hi: Int(33), expLen: 15, expLo: Int(4), expHi: Int(32)},
		}
		for i, test := range tests {
			inputs := makeIntRangeStep(intRangeLo, intRangeHi, intRangeStep)
			tree, _ := NewTree(treeOrder)
			for _, key := range inputs {
				tree.Put(key)
			}
			pivot := test.hi.(Int)
			out := make([]Entry, 0)
			tree.DescendLessOrEqual(pivot, func(ent Entry) bool {
				out = append(out, ent)
				return true
			})
			if ok := testTreeRange(t, out, test.expLen, test.expHi, test.expLo); !ok {
				t.Errorf("Failure, test %d", i)
			}
		}
	})

	t.Run("AscendRange", func(t *testing.T) {
		tests := []testCase{
			{lo: Int(-5), hi: Int(0), expLen: 0},
			{lo: Int(-5), hi: Int(9), expLen: 3, expLo: Int(4), expHi: Int(8)},
			{lo: Int(7), hi: Int(27), expLen: 10, expLo: Int(8), expHi: Int(26)},
			{lo: Int(8), hi: Int(28), expLen: 10, expLo: Int(8), expHi: Int(26)},
			{lo: Int(9), hi: Int(28), expLen: 9, expLo: Int(10), expHi: Int(26)},
			{lo: Int(16), hi: Int(40), expLen: 9, expLo: Int(16), expHi: Int(32)},
			{lo: Int(15), hi: Int(40), expLen: 9, expLo: Int(16), expHi: Int(32)},
			{lo: Int(33), hi: Int(40), expLen: 0},
			{lo: Int(-5), hi: Int(40), expLen: 15, expLo: Int(4), expHi: Int(32)},
		}
		for i, test := range tests {
			inputs := makeIntRangeStep(intRangeLo, intRangeHi, intRangeStep)
			tree, _ := NewTree(treeOrder)
			for _, key := range inputs {
				tree.Put(key)
			}
			lo := test.lo.(Int)
			hi := test.hi.(Int)
			out := make([]Entry, 0)
			tree.AscendRange(lo, hi, func(ent Entry) bool {
				out = append(out, ent)
				return true
			})
			if ok := testTreeRange(t, out, test.expLen, test.expLo, test.expHi); !ok {
				t.Errorf("Failure, test %d", i)
			}
		}
	})

	t.Run("DescendRange", func(t *testing.T) {
		tests := []testCase{
			{lo: Int(-5), hi: Int(0), expLen: 0},
			{lo: Int(-5), hi: Int(9), expLen: 3, expLo: Int(4), expHi: Int(8)},
			{lo: Int(8), hi: Int(28), expLen: 10, expLo: Int(10), expHi: Int(28)},
			{lo: Int(7), hi: Int(27), expLen: 10, expLo: Int(8), expHi: Int(26)},
			{lo: Int(11), hi: Int(19), expLen: 4, expLo: Int(12), expHi: Int(18)},
			{lo: Int(16), hi: Int(40), expLen: 8, expLo: Int(18), expHi: Int(32)},
			{lo: Int(15), hi: Int(40), expLen: 9, expLo: Int(16), expHi: Int(32)},
			{lo: Int(33), hi: Int(40), expLen: 0},
			{lo: Int(-5), hi: Int(40), expLen: 15, expLo: Int(4), expHi: Int(32)},
		}
		for i, test := range tests {
			inputs := makeIntRangeStep(intRangeLo, intRangeHi, intRangeStep)
			tree, _ := NewTree(treeOrder)
			for _, key := range inputs {
				tree.Put(key)
			}
			if i == 0 {
				dumpTreeInTestContext(t, tree)
			}
			lessOrEqual := test.hi.(Int)
			greaterThan := test.lo.(Int)
			out := make([]Entry, 0)
			tree.DescendRange(lessOrEqual, greaterThan, func(ent Entry) bool {
				out = append(out, ent)
				return true
			})
			if ok := testTreeRange(t, out, test.expLen, test.expHi, test.expLo); !ok {
				t.Errorf("Failure, test %d", i)
			}
		}
	})
}

const (
	benchmarkTreeSize  = 10000
	benchmarkTreeOrder = 64
)

func BenchmarkPut(b *testing.B) {
	b.StopTimer()
	inputs := makePerm(benchmarkTreeSize)
	b.StartTimer()
	i := 0
	for i < b.N {
		tree, err := NewTree(benchmarkTreeOrder)
		if err != nil {
			b.Fatalf("Error: %v", err)
		}
		for _, key := range inputs {
			tree.Put(key)
			i++
			if i >= b.N {
				return
			}
		}
	}
}

func BenchmarkDelPut(b *testing.B) {
	b.StopTimer()
	inputs := makePerm(benchmarkTreeSize)
	tree, err := NewTree(benchmarkTreeOrder)
	if err != nil {
		b.Fatalf("Error: %v", err)
	}
	for _, key := range inputs {
		tree.Put(key)
	}
	b.StartTimer()
	var key Entry
	for i := 0; i < b.N; i++ {
		key = inputs[i%benchmarkTreeSize]
		tree.Del(key)
		tree.Put(key)
	}
}

func BenchmarkDel(b *testing.B) {
	b.StopTimer()
	inputs := makePerm(benchmarkTreeSize)
	removeP := makePerm(benchmarkTreeSize)
	b.StartTimer()
	i := 0
	for i < b.N {
		b.StopTimer()
		tree, err := NewTree(benchmarkTreeOrder)
		if err != nil {
			b.Fatalf("Error: %v", err)
		}
		for _, v := range inputs {
			tree.Put(v)
		}
		b.StartTimer()
		for _, item := range removeP {
			tree.Del(item)
			i++
			if i >= b.N {
				return
			}
		}
	}
}

func BenchmarkGet(b *testing.B) {
	b.StopTimer()
	inputs := makePerm(benchmarkTreeSize)
	toRemove := makePerm(benchmarkTreeSize)
	b.StartTimer()
	i := 0
	for i < b.N {
		b.StopTimer()
		tree, err := NewTree(benchmarkTreeOrder)
		if err != nil {
			b.Fatalf("Error: %v", err)
		}
		for _, v := range inputs {
			tree.Put(v)
		}
		b.StartTimer()
		for _, k := range toRemove {
			tree.Get(k)
			i++
			if i >= b.N {
				return
			}
		}
	}
}

func BenchmarkAscend(b *testing.B) {
	arr := makePerm(benchmarkTreeSize)
	tr, err := NewTree(benchmarkTreeOrder)
	if err != nil {
		b.Fatalf("Error: %v", err)
	}
	for _, v := range arr {
		tr.Put(v)
	}
	sort.Sort(byInts(arr))
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		j := 0
		tr.Ascend(func(ent Entry) bool {
			if ent.(Int) != arr[j].(Int) {
				b.Fatalf("mismatch: expected: %v, got %v", arr[j].(Int), ent.(Int))
			}
			j++
			return true
		})
	}
}

func BenchmarkDescend(b *testing.B) {
	arr := makePerm(benchmarkTreeSize)
	tr, err := NewTree(benchmarkTreeOrder)
	if err != nil {
		b.Fatalf("Error: %v", err)
	}
	for _, v := range arr {
		tr.Put(v)
	}
	sort.Sort(byInts(arr))
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		j := len(arr) - 1
		tr.Descend(func(ent Entry) bool {
			if ent.(Int) != arr[j].(Int) {
				b.Fatalf("mismatch: expected: %v, got %v", arr[j].(Int), ent.(Int))
			}
			j--
			return true
		})
	}
}

func BenchmarkAscendGreaterOrEqual(b *testing.B) {
	arr := makePerm(benchmarkTreeSize)
	tr, err := NewTree(benchmarkTreeOrder)
	if err != nil {
		b.Fatalf("Error: %v", err)
	}
	for _, v := range arr {
		tr.Put(v)
	}
	sort.Sort(byInts(arr))
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		j := 100
		k := 0
		tr.AscendGreaterOrEqual(Int(100), func(ent Entry) bool {
			if ent.(Int) != arr[j].(Int) {
				b.Fatalf("mismatch: expected: %v, got %v", arr[j].(Int), ent.(Int))
			}
			j++
			k++
			return true
		})
		if j != len(arr) {
			b.Fatalf("expected: %v, got %v\n", len(arr), j)
		}
		if k != len(arr)-100 {
			b.Fatalf("expected: %v, got %v\n", len(arr)-100, k)
		}
	}
}

func BenchmarkDescendLessOrEqual(b *testing.B) {
	arr := makePerm(benchmarkTreeSize)
	tr, err := NewTree(benchmarkTreeOrder)
	if err != nil {
		b.Fatalf("Error: %v", err)
	}
	for _, v := range arr {
		tr.Put(v)
	}
	sort.Sort(byInts(arr))
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		j := len(arr) - 100
		k := len(arr)
		tr.DescendLessOrEqual(arr[len(arr)-100], func(ent Entry) bool {
			if ent.(Int) != arr[j].(Int) {
				b.Fatalf("mismatch: expected: %v, got %v", arr[j].(Int), ent.(Int))
			}
			j--
			k--
			return true
		})
		if j != -1 {
			b.Fatalf("expected: %v, got %v\n", -1, j)
		}
		if k != 99 {
			b.Fatalf("expected: %v, got %v\n", 99, k)
		}
	}
}

func BenchmarkAscendRange(b *testing.B) {
	arr := makePerm(benchmarkTreeSize)
	tr, err := NewTree(benchmarkTreeOrder)
	if err != nil {
		b.Fatalf("Error: %v", err)
	}
	for _, v := range arr {
		tr.Put(v)
	}
	sort.Sort(byInts(arr))
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		j := 100
		tr.AscendRange(Int(100), arr[len(arr)-100], func(item Entry) bool {
			if item.(Int) != arr[j].(Int) {
				b.Fatalf("mismatch: expected: %v, got %v", arr[j].(Int), item.(Int))
			}
			j++
			return true
		})
		if j != len(arr)-100 {
			b.Fatalf("expected: %v, got %v", len(arr)-100, j)
		}
	}
}

func BenchmarkDescendRange(b *testing.B) {
	arr := makePerm(benchmarkTreeSize)
	tr, err := NewTree(benchmarkTreeOrder)
	if err != nil {
		b.Fatalf("Error: %v", err)
	}
	for _, v := range arr {
		tr.Put(v)
	}
	sort.Sort(byInts(arr))
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		j := len(arr) - 100
		tr.DescendRange(arr[len(arr)-100], Int(100), func(item Entry) bool {
			if item.(Int) != arr[j].(Int) {
				b.Fatalf("mismatch: expected: %v, got %v", arr[j].(Int), item.(Int))
			}
			j--
			return true
		})
		if j != 100 {
			b.Fatalf("expected: %v, got %v", 100, j)
		}
	}
}
