package bpt

import (
	"testing"
)

func TestInternalNode(t *testing.T) {
	t.Run("findKeyInd", func(t *testing.T) {
		tests := []struct {
			key           Entry
			expectedInd   int
			expectedFound bool
		}{
			{key: Int(2), expectedInd: 0, expectedFound: true},
			{key: Int(4), expectedInd: 1, expectedFound: true},
			{key: Int(6), expectedInd: 2, expectedFound: true},
			{key: Int(8), expectedInd: 3, expectedFound: true},
			{key: Int(10), expectedInd: 4, expectedFound: false}, // stops just short of this value
			{key: Int(1), expectedInd: 0, expectedFound: false},
			{key: Int(3), expectedInd: 1, expectedFound: false},
			{key: Int(5), expectedInd: 2, expectedFound: false},
			{key: Int(7), expectedInd: 3, expectedFound: false},
			{key: Int(9), expectedInd: 4, expectedFound: false},
			{key: Int(11), expectedInd: 4, expectedFound: false},
		}

		node := newInternalNode(5)
		node.keys = []Entry{Int(2), Int(4), Int(6), Int(8), Int(10)}
		node.count = len(node.keys)

		for i, test := range tests {
			actual, found := node.searchKeyIndex(test.key)
			if actual != test.expectedInd {
				t.Errorf("Test %d input %d. Got %d, expected %d", i, test.key, actual, test.expectedInd)
			}
			if found != test.expectedFound {
				t.Errorf("Test %d input %d, . Got %t, expected %t", i, test.key, found, test.expectedFound)
			}
		}
	})
}

func TestLeafNode(t *testing.T) {
	t.Run("findKeyInd", func(t *testing.T) {
		tests := []struct {
			key           Entry
			expectedInd   int
			expectedFound bool
		}{
			{key: Int(2), expectedInd: 0, expectedFound: true},
			{key: Int(4), expectedInd: 1, expectedFound: true},
			{key: Int(6), expectedInd: 2, expectedFound: true},
			{key: Int(8), expectedInd: 3, expectedFound: true},
			{key: Int(10), expectedInd: 4, expectedFound: true},
			{key: Int(1), expectedInd: 0, expectedFound: false},
			{key: Int(3), expectedInd: 1, expectedFound: false},
			{key: Int(5), expectedInd: 2, expectedFound: false},
			{key: Int(7), expectedInd: 3, expectedFound: false},
			{key: Int(9), expectedInd: 4, expectedFound: false},
			{key: Int(11), expectedInd: 5, expectedFound: false},
		}

		node := newLeafNode(5)
		node.keys = []Entry{Int(2), Int(4), Int(6), Int(8), Int(10)}
		node.count = len(node.keys)

		for i, test := range tests {
			actual, found := node.searchKeyIndex(test.key)
			if actual != test.expectedInd {
				t.Errorf("Test %d input %d. Got %d, expected %d", i, test.key, actual, test.expectedInd)
			}
			if found != test.expectedFound {
				t.Errorf("Test %d input %d, . Got %t, expected %t", i, test.key, found, test.expectedFound)
			}
		}
	})
}
