package bpt_test

import (
	"fmt"

	"bitbucket.org/rafaelespinoza/bpt"
)

func ExampleTree() {
	inputKeys := []bpt.Int{3, 18, 14, 24, 72, 1, 39, 53, 63, 90, 88, 15, 10, 44, 68, 74}

	tree, err := bpt.NewTree(4)
	if err != nil {
		panic(err)
	}
	for _, num := range inputKeys {
		tree.Put(num)
	}
	bpt.DumpTree(tree)
	// Output:
	// node: 63
	// +-------node: 14 18 39
	// |       +-------leaf: 1 3 10
	// |       +-------leaf: 14 15
	// |       +-------leaf: 18 24
	// |       +-------leaf: 39 44 53
	// +-------node: 72 88
	//         +-------leaf: 63 68
	//         +-------leaf: 72 74
	//         +-------leaf: 88 90
}

func ExampleTree_Del() {
	tree, err := bpt.NewTree(4)
	if err != nil {
		panic(err)
	}
	for i := 1; i <= 16; i++ {
		tree.Put(bpt.Int(i))
	}
	toRemove := []bpt.Int{4, 6, 13, 9}
	fmt.Println("Initial tree")
	bpt.DumpTree(tree)
	for _, num := range toRemove {
		fmt.Printf("removing %d\n", num)
		tree.Del(num)
		bpt.DumpTree(tree)
	}
	// Output:
	// Initial tree
	// node: 7 13
	// +-------node: 3 5
	// |       +-------leaf: 1 2
	// |       +-------leaf: 3 4
	// |       +-------leaf: 5 6
	// +-------node: 9 11
	// |       +-------leaf: 7 8
	// |       +-------leaf: 9 10
	// |       +-------leaf: 11 12
	// +-------node: 15
	//         +-------leaf: 13 14
	//         +-------leaf: 15 16
	// removing 4
	// node: 7 13
	// +-------node: 5
	// |       +-------leaf: 1 2 3
	// |       +-------leaf: 5 6
	// +-------node: 9 11
	// |       +-------leaf: 7 8
	// |       +-------leaf: 9 10
	// |       +-------leaf: 11 12
	// +-------node: 15
	//         +-------leaf: 13 14
	//         +-------leaf: 15 16
	// removing 6
	// node: 7 13
	// +-------node: 3
	// |       +-------leaf: 1 2
	// |       +-------leaf: 3 5
	// +-------node: 9 11
	// |       +-------leaf: 7 8
	// |       +-------leaf: 9 10
	// |       +-------leaf: 11 12
	// +-------node: 15
	//         +-------leaf: 13 14
	//         +-------leaf: 15 16
	// removing 13
	// node: 7 11
	// +-------node: 3
	// |       +-------leaf: 1 2
	// |       +-------leaf: 3 5
	// +-------node: 9
	// |       +-------leaf: 7 8
	// |       +-------leaf: 9 10
	// +-------node: 13
	//         +-------leaf: 11 12
	//         +-------leaf: 14 15 16
	// removing 9
	// node: 11
	// +-------node: 3 7
	// |       +-------leaf: 1 2
	// |       +-------leaf: 3 5
	// |       +-------leaf: 7 8 10
	// +-------node: 13
	//         +-------leaf: 11 12
	//         +-------leaf: 14 15 16
}
