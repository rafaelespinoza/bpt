// Package bpt is an in-memory implementation of a B+ Tree. It is largely a
// remake of this B+ Tree implementation in C:
// https://github.com/begeekmyfriend/bplustree/tree/in-memory
package bpt

import (
	"container/list"
	"fmt"
)

// Tree is a B+ Tree implementation. It stores Entry values in internal nodes
// and leaf nodes.
type Tree struct {
	order   int
	entries int
	level   int
	root    vertex
	lists   []*list.List
	length  int
}

// NewTree constructs a Tree value and returns a pointer to it. The order
// parameter sets the maximum number of child nodes per internal node.
func NewTree(order int) (*Tree, error) {
	var tree Tree

	if order > maxOrder {
		err := fmt.Errorf("order must be <= %d", maxOrder)
		return &tree, err
	}

	tree.order = order
	tree.entries = order - 1
	tree.lists = make([]*list.List, maxLevel)
	for i := 0; i < maxLevel; i++ {
		tree.lists[i] = list.New()
	}
	return &tree, nil
}

// Get searches the tree for a matching key. It returns the Entry if found,
// otherwise it returns nil.
func (t *Tree) Get(key Entry) Entry {
	if key == nil {
		return nil
	}
	found, value := t.search(key)
	if found {
		return value
	}
	return nil
}

// Min searches the tree for the lowest Entry and returns it. If the tree is
// empty, then it returns nil.
func (t *Tree) Min() Entry {
	curr := t.root

	for curr != nil {
		if isLeaf(&curr) {
			leaf := curr.(*leafNode)
			return leaf.keys[0]
		}

		node := curr.(*internalNode)
		curr = node.nodes[0]
	}

	return nil
}

// Max searches the tree for the highest Entry and returns it. If the tree
// is empty, then it returns nil.
func (t *Tree) Max() Entry {
	curr := t.root

	for curr != nil {
		if isLeaf(&curr) {
			leaf := curr.(*leafNode)
			return leaf.keys[leaf.count-1]
		}

		node := curr.(*internalNode)
		curr = node.nodes[node.count-1]
	}

	return nil
}

// Put adds or updates an Entry on the tree. If adding a new Entry, then it
// returns nil. If updating, then it returns the updated Entry. Adding nil will
// result in a runtime panic.
func (t *Tree) Put(key Entry) Entry {
	if key == nil {
		panic("Cannot insert nil")
	}

	curr := t.root

	for curr != nil {
		if isLeaf(&curr) {
			leaf := curr.(*leafNode)
			return t.insertLeaf(leaf, key)
		}
		node := curr.(*internalNode)
		ind, found := node.searchKeyIndex(key)

		if found {
			curr = node.nodes[ind+1]
		} else {
			curr = node.nodes[ind]
		}
	}

	// init new root
	root := newLeafNode(t.entries)
	root.keys[0] = key
	root.count = 1
	root.link = &link{
		Element: t.lists[t.level].PushFront(root),
		List:    t.lists[t.level],
	}
	t.root = root
	t.length++
	return nil
}

// Del removes an Entry equivalent to key from the tree and returns it. If key
// is not found, it returns nil.
func (t *Tree) Del(key Entry) Entry {
	if key == nil {
		return nil
	}
	curr := t.root

	for curr != nil {
		if isLeaf(&curr) {
			leaf := curr.(*leafNode)
			return t.removeLeaf(leaf, key)
		}

		node := curr.(*internalNode)
		ind, found := node.searchKeyIndex(key)
		if found {
			curr = node.nodes[ind+1]
		} else {
			curr = node.nodes[ind]
		}
	}

	return nil
}

// DelMin removes the lowest Entry from the tree and returns it. If the tree is
// empty, then it returns nil.
func (t *Tree) DelMin() Entry {
	key := t.Min()
	return t.Del(key)
}

// DelMax removes the highest Entry from the tree and returns it. If the tree is
// empty, then it returns nil.
func (t *Tree) DelMax() Entry {
	key := t.Max()
	return t.Del(key)
}

// Ascend calls onEntry for every value in the tree within the range
// [first, last], until the Iterator function returns false.
func (t *Tree) Ascend(onEntry Iterator) {
	min := t.Min()
	t.ascend(min, nil, onEntry)
}

// AscendGreaterOrEqual calls onEntry for every value in the tree within the
// range [pivot, last], until the Iterator function returns false.
func (t *Tree) AscendGreaterOrEqual(pivot Entry, onEntry Iterator) {
	t.ascend(pivot, nil, onEntry)
}

// AscendLessThan calls onEntry for every value in the tree within the
// range [first, pivot), until the Iterator function returns false.
func (t *Tree) AscendLessThan(pivot Entry, onEntry Iterator) {
	min := t.Min()
	t.ascend(min, pivot, onEntry)
}

// AscendRange calls onEntry for every value in the tree within the range
// [greaterOrEqual, lessThan), until the Iterator function returns false.
func (t *Tree) AscendRange(greaterOrEqual, lessThan Entry, onEntry Iterator) {
	t.ascend(greaterOrEqual, lessThan, onEntry)
}

// Descend calls onEntry for every value in the tree within the range
// [last, first], until the Iterator function returns false.
func (t *Tree) Descend(onEntry Iterator) {
	max := t.Max()
	t.descend(max, nil, onEntry)
}

// DescendGreaterThan calls onEntry for every value in the tree within the range
// (pivot, last], until the Iterator function returns false.
func (t *Tree) DescendGreaterThan(pivot Entry, onEntry Iterator) {
	max := t.Max()
	t.descend(max, pivot, onEntry)
}

// DescendLessOrEqual calls onEntry for every value in the tree within the
// range [pivot, first], until the Iterator function returns false.
func (t *Tree) DescendLessOrEqual(pivot Entry, onEntry Iterator) {
	t.descend(pivot, nil, onEntry)
}

// DescendRange calls onEntry for every value in the tree within the range
// [lessOrEqual, greaterThan), until the Iterator function returns false.
func (t *Tree) DescendRange(lessOrEqual, greaterThan Entry, onEntry Iterator) {
	t.descend(lessOrEqual, greaterThan, onEntry)
}

// Len returns the number of Entry values in the tree.
func (t *Tree) Len() int {
	return t.length
}

func (t *Tree) search(key Entry) (found bool, value Entry) {
	curr := t.root

	for curr != nil {
		var ind int

		if isLeaf(&curr) {
			leaf := curr.(*leafNode)
			ind, found = leaf.searchKeyIndex(key)
			if found {
				value = leaf.keys[ind]
			}
			return
		}

		node := curr.(*internalNode)
		ind, found = node.searchKeyIndex(key)
		if found {
			curr = node.nodes[ind+1]
		} else {
			curr = node.nodes[ind]
		}
	}

	return
}

func (t *Tree) insertLeaf(leaf *leafNode, key Entry) Entry {
	// search key location
	insertInd, found := leaf.searchKeyIndex(key)
	if found {
		leaf.insert(key, insertInd)
		return key // already exists
	}

	if leaf.count != t.entries {
		leaf.insert(key, insertInd)
		t.length++
		return nil
	}

	// leaf node is full, must split it
	splitInd := cut(t.entries)
	sib := newLeafNode(t.entries)
	if insertInd < splitInd {
		leaf.addLeftLink(sib)
		leaf.splitLeft(sib, key, insertInd)
	} else {
		leaf.addRightLink(sib)
		leaf.splitRight(sib, key, insertInd)
	}

	t.length++

	// build new parent node
	if insertInd < splitInd {
		return t.buildParentNode(sib, leaf, leaf.keys[0], 0)
	}

	return t.buildParentNode(leaf, sib, sib.keys[0], 0)
}

func (t *Tree) buildParentNode(left vertex, right vertex, key Entry, level int) Entry {
	if left.Parent() == nil && right.Parent() == nil {
		// make new parent
		parent := newInternalNode(t.entries)
		parent.keys[0] = key
		left.SetParent(parent)
		left.SetParentKeyInd(-1)
		right.SetParent(parent)
		right.SetParentKeyInd(0)
		parent.nodes[0] = left
		parent.nodes[1] = right
		parent.count = 2

		// update root
		t.root = parent
		t.level++
		parent.link = &link{
			Element: t.lists[t.level].PushFront(parent),
			List:    t.lists[t.level],
		}
		return nil
	}

	if right.Parent() == nil {
		parent := left.Parent()
		right.SetParent(parent)
		return t.insertInternal(parent, left, right, key, level+1)
	}

	parent := right.Parent()
	left.SetParent(parent)
	return t.insertInternal(parent, left, right, key, level+1)
}

func (t *Tree) insertInternal(parent *internalNode, leftCh vertex, rightCh vertex, key Entry, level int) Entry {
	insertInd, found := parent.searchKeyIndex(key)
	if found {
		DumpTree(t)
		err := fmt.Sprintf("Key %d must be absent. Got %d for key index", key, insertInd)
		panic(err)
	}

	if parent.count != t.order {
		parent.insert(leftCh, rightCh, key, insertInd)
		return nil
	}

	// node is full, must split it
	splitInd := cut(parent.count)
	sib := newInternalNode(len(parent.keys)) // sibling of parent node
	var splitKey Entry
	if insertInd < splitInd {
		parent.addLeftLink(sib)
		splitKey = parent.splitLeft(sib, leftCh, rightCh, key, insertInd)
	} else if insertInd == splitInd {
		parent.addRightLink(sib)
		splitKey = parent.splitRight1(sib, leftCh, rightCh, key, insertInd)
	} else {
		parent.addRightLink(sib)
		splitKey = parent.splitRight2(sib, leftCh, rightCh, key, insertInd)
	}

	// build new parent node
	if insertInd < splitInd {
		return t.buildParentNode(sib, parent, splitKey, level)
	}

	return t.buildParentNode(parent, sib, splitKey, level)
}

func (t *Tree) removeLeaf(leaf *leafNode, key Entry) (out Entry) {
	// search key location
	removeInd, found := leaf.searchKeyIndex(key)
	if !found {
		return
	}
	floor := cut(t.entries)
	if leaf.count > floor {
		out = leaf.keys[removeInd]
		leaf.remove(removeInd)
		t.length--
		return
	}

	parent := leaf.parent

	if parent == nil {
		if leaf.count != 1 {
			out = leaf.keys[removeInd]
			leaf.remove(removeInd)
			t.length--
			return
		}
		// delete only last node
		if key != leaf.keys[0] {
			err := fmt.Errorf("key %d != leaf.Keys[0]. Trying to delete wrong key", key)
			panic(err)
		}
		t.root = nil
		t.lists[t.level].Remove(leaf.Link().Element)
		t.length--
		return leaf.keys[0]
	}

	parentKeyInd := leaf.parentKeyInd
	if leaf.whichBorrowee() == siblingToLeft {
		leftSib := leaf.prev()
		out = leaf.keys[removeInd]
		if leftSib.count > floor {
			leaf.shiftFromLeft(leftSib, parentKeyInd, removeInd)
		} else {
			leaf.mergeIntoLeft(leftSib, removeInd)
			leftSib.removeLink(leaf)
			t.removeInternal(parent, parentKeyInd)
		}
	} else {
		rightSib := leaf.next()
		out = leaf.keys[removeInd]
		// remove first in case of overflow during the merge w/ sibling
		leaf.remove(removeInd)
		if rightSib.count > floor {
			leaf.shiftFromRight(rightSib, parentKeyInd+1)
		} else {
			leaf.mergeFromRight(rightSib)
			leaf.removeLink(rightSib)
			t.removeInternal(parent, parentKeyInd+1)
		}
	}

	t.length--
	return
}

func (t *Tree) removeInternal(node *internalNode, removeInd int) {
	floor := cut(t.order)
	if node.count > floor {
		node.remove(removeInd)
		return
	}

	parent := node.parent

	if parent == nil {
		if node.count != 2 {
			node.remove(removeInd)
			return
		}
		if removeInd != 0 {
			panic("should not remove first node")
		}
		// delete old root node
		node.nodes[0].SetParent(nil)
		t.root = node.nodes[0]
		t.lists[t.level].Remove(node.Link().Element)
		t.level--

		return
	}

	parentKeyInd := node.parentKeyInd
	if node.whichBorrowee() == siblingToLeft {
		leftSib := node.prev()
		if leftSib.count > floor {
			node.shiftFromLeft(leftSib, parentKeyInd, removeInd)
		} else {
			node.mergeIntoLeft(leftSib, parentKeyInd, removeInd)
			leftSib.removeLink(node)
			t.removeInternal(parent, parentKeyInd)
		}
	} else {
		rightSib := node.next()
		// remove first in case of overflow during the merge w/ sibling
		node.remove(removeInd)
		if rightSib.count > floor {
			node.shiftFromRight(rightSib, parentKeyInd+1)
		} else {
			node.mergeFromRight(rightSib, parentKeyInd+1)
			node.removeLink(rightSib)
			t.removeInternal(parent, parentKeyInd+1)
		}
	}
}

// Iterator allows finer-grained iteration over Entry values in the tree. It's
// used as an parameter to Tree's Ascend* or Descend* methods. Iteration
// stops once this function returns false.
type Iterator func(Entry) bool

func (t *Tree) ascend(start, stop Entry, onEntry Iterator) {
	var node *internalNode
	var leaf *leafNode
	var i int
	var found bool
	curr := t.root

	for !isLeaf(&curr) {
		node = curr.(*internalNode)
		if i, found = node.searchKeyIndex(start); found {
			curr = node.nodes[i+1]
		} else {
			curr = node.nodes[i]
		}
	}

	leaf = curr.(*leafNode)
	i, found = leaf.searchKeyIndex(start)
	if !found && i >= leaf.count {
		leaf = leaf.next()
		i = 0
	}

	for leaf != nil {
		if stop != nil && !leaf.keys[i].Less(stop) {
			return
		}
		if !onEntry(leaf.keys[i]) {
			return
		}
		i++
		if i >= leaf.count {
			leaf = leaf.next()
			i = 0
		}
	}
}

func (t *Tree) descend(start, stop Entry, onEntry Iterator) {
	var node *internalNode
	var leaf *leafNode
	var i int
	var found bool
	curr := t.root

	for !isLeaf(&curr) {
		node = curr.(*internalNode)
		if i, found = node.searchKeyIndex(start); found {
			curr = node.nodes[i+1]
		} else {
			curr = node.nodes[i]
		}
	}

	leaf = curr.(*leafNode)
	i, found = leaf.searchKeyIndex(start)
	if !found {
		if i >= leaf.count {
			i = leaf.count - 1
		} else if i == 0 {
			leaf = leaf.prev()
			if leaf != nil {
				i = leaf.count - 1
			}
		} else if leaf != nil && !leaf.keys[i].Less(start) {
			i--
		}
	}

	for leaf != nil {
		if stop != nil && !stop.Less(leaf.keys[i]) {
			return
		}
		if !onEntry(leaf.keys[i]) {
			return
		}
		i--
		if i < 0 {
			leaf = leaf.prev()
			if leaf != nil {
				i = leaf.count - 1
			}
		}
	}
}
